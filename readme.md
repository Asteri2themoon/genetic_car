### Physique simplifié

Les voitures sont contrôlées sur deux axes.
- un axe qui accélère le véhicule quand il est positif et freine quand il est négatif.
- un axe qui contrôle le virage (négatif d'un côté et positif de l'autre).

la puissance maximale de l'accélération, du freinage et du virage est limitée à une constante fixe pour toutes les voitures.
Des forces de frottement s'opposent à l'avancer du véhicule pour rendre son comportement plus réaliste.

### Capteur

Chaque voiture a trois capteurs à l'avant (symbolisés par des traits verts), un au centre dès un de chaque côté pour pouvoir avoir un aperçu de son environnement.
Chaque capteur renvoie un nombre entre 0 et 1 qui représente la distance de détection du capteur (renvoie 1 quand rien n'est détecté).

### Algorithme génétique

Chaque intelligence artificielle possède un génome différent qui définit la façon dont elle se comporte. Le génome est composé de 8 réels. La puissance et le virage sont calculés avec l'expression ci-dessous:
```python
power=genome[0]*sensor[0]+genome[1]*sensor[1]+genome[2]*sensor[2]+genome[3]
turn=genome[4]/10*sensor[0]+genome[5]/10*sensor[1]+genome[6]/10*sensor[2]+genome[7]/10
```


La première génération est créée aléatoirement avec une distribution normale. Les six premiers véhicules sont sélectionnés à la fin de chaque test et les mutations des gènes sont aussi générées aléatoirement avec une distribution normale.

[résultat en vidéo](https://www.youtube.com/watch?v=oYBY7dQ5AxI)