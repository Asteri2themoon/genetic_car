import pygame
from pygame.locals import *
import math
import time
import numpy as np
import matplotlib.pyplot as plt

carWidth,carLength,carNose=15,30,5
carPoints=[[-carWidth/2,-carLength/2],[carWidth/2,-carLength/2],[carWidth/2,carLength/2],[0,carLength/2+carNose],[-carWidth/2,carLength/2],
		[-4,carLength/2+4],[-24,carLength/2+30],[4,carLength/2+4],[24,carLength/2+30],[0,carLength/2+carNose+1],[0,carLength/2+carNose+33]]
carLines=[[0,1],[1,2],[2,3],[3,4],[2,4],[0,4],[5,6],[7,8],[9,10]]
carColor=[(255,0,0),(255,0,0),(255,0,0),(255,0,0),(255,0,0),(255,0,0),(0,255,0),(0,255,0),(0,255,0)]

def solve(A,B,C,D):
	min,max=np.minimum(A,B),np.maximum(A,B)#optimisation
	if (C[0]<min[0] and D[0]<min[0]) or (C[1]<min[1] and D[1]<min[1]) or (C[0]>max[0] and D[0]>max[0]) or (C[1]>max[1] and D[1]>max[1]):
		return -1
	if np.dot(np.subtract(B,A),np.subtract(D,C))==0:#colinearity test
		if np.dot(np.subtract(B,A),np.subtract(C,A))==0:
			return 0
		return -1
	if C[0]==D[0]:#calculate intersection
		t=(C[0]-A[0])/(B[0]-A[0])
		tp=(A[1]-t*(B[1]-A[1])-C[1])/(D[1]-C[1])
	else:
		k=B[1]-A[1]-(D[1]-C[1])*(B[0]-A[0])/(D[0]-C[0])
		t=(-A[1]+C[1]+(D[1]-C[1])*(A[0]-C[0])/(D[0]-C[0]))/k
		tp=(A[0]+t*(B[0]-A[0])-C[0])/(D[0]-C[0])
	if t>=0 and t<=1 and tp>=0 and tp<=1:
		return t
	return -1

def getTransformationMatrix(dx,dy,da):
	sina,cosa=math.sin(da),math.cos(da)
	return np.matrix([[cosa,sina],[sina,-cosa],[dx,dy]])

def renderer(screen,points,lines,colors,transform=[]):
	if len(transform)>0:
		tPoints=[]
		for point in points:
			p=[point[0],point[1],1]
			tPoints.extend(np.asarray(np.matmul(p,transform)))
	else:
		tPoints=points
	for i in range(len(lines)):
		pygame.draw.line(screen,colors[i],tuple(tPoints[lines[i][0]]),tuple(tPoints[lines[i][1]]))
	return tPoints

def getCollision(screen,road,model):
	roadPoints,roadLines=road
	[modelPoints,modelLines,_]=model
	collision=[]
	for mLine in modelLines:
		min=2
		for rLine in roadLines:
			t=solve(modelPoints[mLine[0]],modelPoints[mLine[1]],roadPoints[rLine[0]],roadPoints[rLine[1]])
			if t>=0 and t<=1:
				intersection=np.add(modelPoints[mLine[0]],np.multiply(t,np.subtract(modelPoints[mLine[1]],modelPoints[mLine[0]])))
				pygame.draw.circle(screen,(255,255,0),(int(intersection[0]),int(intersection[1])),2)
				pygame.draw.line(screen,(255,0,255),roadPoints[rLine[0]],roadPoints[rLine[1]])
				if t<min:
					min=t
		if min==2:
			collision.append(-1)
		else:
			collision.append(min)
	return collision

def drawCar(screen,car):
	mat=getTransformationMatrix(car['x'],car['y'],car['a'])
	(points,lines,colors)=car['model']
	return renderer(screen,points,lines,colors,transform=mat)

def genBezier(screen,p0,p1,p2,p3,width=40,line=30,color=(0,0,255),points=[],lines=[],colors=[]):
	offset=len(points)
	for t in np.arange(0,(1+line)/line,1/line):
		newP=np.add(np.add(np.add(np.multiply((1-t)**3,p0),np.multiply(3*t*(1-t)**2,p1)),np.multiply(3*t**2*(1-t),p2)),np.multiply(t**3,p3))
		speed=np.add(np.add(np.add(np.multiply(-3*t**2+6*t-3,p0),np.multiply(3*(3*t**2-4*t+1),p1)),np.multiply(3*(-3*t**2+2*t),p2)),np.multiply(3*t**2,p3))
		length=math.sqrt(np.dot(speed,speed))
		vect=np.multiply(1/length,[speed[1],-speed[0]])
		points.append(tuple(np.add(newP,np.multiply(width/2,vect))))
		points.append(tuple(np.add(newP,np.multiply(-width/2,vect))))
	for i in range(line):
		lines.append([offset+i*2,offset+i*2+2])
		lines.append([offset+i*2+1,offset+i*2+3])
		colors.append(color)
		colors.append(color)
	return points,lines,colors

def physic(car,dt):
	if car['collision']:
		return car
	if car['power']>1:
		car['power']=1.0
	elif car['power']<-1:
		car['power']=-1.0
	if car['da']>0.03:
		car['da']=0.03
	elif car['da']<-0.03:
		car['da']=-0.03
	car['speed']+=30*car['power']*dt-0.02*car['speed']
	if car['speed']<0.0:
		car['speed']=0.0
	car['x']+=math.sin(car['a'])*car['speed']*dt
	car['y']+=-math.cos(car['a'])*car['speed']*dt
	car['a']+=car['da']*car['speed']*dt
	car['distance']+=car['speed']*dt
	return car

def ai(car):
	sensor=car['sensor']
	genome=car['genome']
	car['power']=genome[0]*sensor[0]+genome[1]*sensor[1]+genome[2]*sensor[2]+genome[3]
	car['da']=genome[4]/10*sensor[0]+genome[5]/10*sensor[1]+genome[6]/10*sensor[2]+genome[7]/10
	return car

def randomInit(n,car):
	cars=[]
	for i in range(n):
		cars.append(car.copy())
		cars[-1]['genome']=0.1*np.random.randn(8)
	return cars

def getMutation(genome,stdDeviation):
	return genome+stdDeviation*np.random.randn(8)

def getNextGen(cars,car):
	best=sorted(cars, key=lambda k: k['distance'], reverse=True)
	print("best score:",best[0]['distance'])
	child=[0,0,0,0,0,1,1,1,1,2,2,2,3,3,4,5]
	newCar=[]
	for i in child:
		newCar.append(car.copy())
		newCar[-1]['genome']=getMutation(best[i]['genome'],0.05)
	return newCar,best[0]['distance']

pygame.init()
screen = pygame.display.set_mode((640, 480))
font = pygame.font.SysFont('Arial', 20)

continuer = True
t,dt,crash,n,generation,bestScore=0,0.1,0,16,1,0

bPoints,bLines,bColors=genBezier(screen,[250,150],[350,150],[450,100],[450,200])
bPoints,bLines,bColors=genBezier(screen,[450,200],[450,300],[300,200],[300,300],points=bPoints,lines=bLines,colors=bColors)
bPoints,bLines,bColors=genBezier(screen,[300,300],[300,500],[550,450],[550,300],points=bPoints,lines=bLines,colors=bColors)
bPoints,bLines,bColors=genBezier(screen,[550,300],[550,50],[550,50],[250,50],points=bPoints,lines=bLines,colors=bColors)
bPoints,bLines,bColors=genBezier(screen,[250,50],[50,50],[50,150],[50,350],points=bPoints,lines=bLines,colors=bColors)
bPoints,bLines,bColors=genBezier(screen,[50,350],[50,450],[150,450],[150,350],points=bPoints,lines=bLines,colors=bColors)
bPoints,bLines,bColors=genBezier(screen,[150,350],[150,250],[150,150],[250,150],points=bPoints,lines=bLines,colors=bColors)

defaultCar={'x':250.0,'y':150.0,'a':1.5,'speed':0.0,'da':0.0,'power':0.0,'distance':0,'model':[carPoints,carLines,carColor],'sensor':[1.0,1.0,1.0],'collision':False}
cars=randomInit(n,defaultCar)
pygame.display.set_caption('genetic car')

begin=time.time()+dt
while continuer:
	for event in pygame.event.get():
		if event.type == QUIT:
			continuer = False
	
	screen.fill((0, 0, 0))
	renderer(screen,bPoints,bLines,bColors)
	for car in cars:
		carModel=drawCar(screen,car)
		if car['collision']==False:
			collision=getCollision(screen,(bPoints,bLines),[carModel,car['model'][1],car['model'][2]])
			end=False
			for i in range(6):
				if collision[i]!=-1 and car['collision']==False:
					car['collision']=True
					crash+=1
			for i in range(6,9):
				if collision[i]==-1:
					collision[i]=1
			car['sensor']=collision[6:]
			car=ai(car)
			car=physic(car,dt)
	if len(cars)>0:
		t+=dt
		if t>30 or crash>=n:
			t,crash=0,0
			cars,best=getNextGen(cars,defaultCar)
			if best>bestScore:
				bestScore=best
			generation+=1
	screen.blit(font.render('generation: {0}'.format(generation), True, (100,0,0)),(0,0))
	screen.blit(font.render('operating time: {0:.2f}s'.format(t), True, (100,0,0)),(0,25))
	screen.blit(font.render('beste score: {0:.1f}'.format(bestScore), True, (100,0,0)),(0,50))
	pygame.display.flip()
